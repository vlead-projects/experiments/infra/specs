#+TITLE: Experiment Intermediate
#+AUTHOR: VLEAD
#+DATE: [2018-07-18 Wed]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* BNF Grammar
#+BEGIN_EXAMPLE

document  :: title outline-2 + ..

outline-2 ::= id h2 outline-text-2 outline-3 * ..

h2        ::= id span-class text

outline-text-2 ::= id pre-tag

outline-3 ::= h3 outline-text-3 outline-4 * ..

h3 ::= id span-class text

outline-text-3 ::= id pre-tag

outline-4 ::= id h4 outline-text-4 

h4 ::= id span-class text

outline-text-3 ::= id pre-tag p-tag* list-elem*

id ::= string

pre-tag ::= (key value) + ..

p-tag ::= text
#+END_EXAMPLE

